﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Documentaciya
{
    public partial class frmDop : Form
    {
        public frmDop()
        {
            InitializeComponent();
        }

        private void dopolnenieBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.dopolnenieBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dBDocDataSet);

        }

        private void frmDop_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "dBDocDataSet.Dopolnenie". При необходимости она может быть перемещена или удалена.
            this.dopolnenieTableAdapter.Fill(this.dBDocDataSet.Dopolnenie);

        }
    }
}
