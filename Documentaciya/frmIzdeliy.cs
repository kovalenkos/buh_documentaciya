﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Documentaciya
{
    public partial class frmIzdeliy : Form
    {
        public frmIzdeliy()
        {
            InitializeComponent();
           
        }
      
        private void izdelieBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.izdelieBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dBDocDataSet);
            Application.Restart();
        }

        private void frmIzdeliy_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "dBDocDataSet.Izdelie". При необходимости она может быть перемещена или удалена.
            this.izdelieTableAdapter.Fill(this.dBDocDataSet.Izdelie);

        }

        private void frmIzdeliy_FormClosed(object sender, FormClosedEventArgs e)
        {
           
        }

        private void izdelieDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (izdelieDataGridView.Columns[e.ColumnIndex].Name == "Delete")
            {
                if (MessageBox.Show("Вы уверены что хотите удалить?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    izdelieBindingSource.RemoveCurrent();
            }
        }
    }
}
