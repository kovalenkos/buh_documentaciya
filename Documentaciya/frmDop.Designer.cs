﻿namespace Documentaciya
{
    partial class frmDop
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label nomerPPLabel;
            System.Windows.Forms.Label dateLabel;
            System.Windows.Forms.Label sodIzmLabel;
            System.Windows.Forms.Label fIOLabel;
            System.Windows.Forms.Label primechanieLabel;
            System.Windows.Forms.Label nomIzvLabel;
            this.dBDocDataSet = new Documentaciya.DBDocDataSet();
            this.dopolnenieBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dopolnenieTableAdapter = new Documentaciya.DBDocDataSetTableAdapters.DopolnenieTableAdapter();
            this.tableAdapterManager = new Documentaciya.DBDocDataSetTableAdapters.TableAdapterManager();
            this.nomerPPTextBox = new System.Windows.Forms.TextBox();
            this.dateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.sodIzmTextBox = new System.Windows.Forms.TextBox();
            this.fIOTextBox = new System.Windows.Forms.TextBox();
            this.primechanieTextBox = new System.Windows.Forms.TextBox();
            this.nomIzvTextBox = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            nomerPPLabel = new System.Windows.Forms.Label();
            dateLabel = new System.Windows.Forms.Label();
            sodIzmLabel = new System.Windows.Forms.Label();
            fIOLabel = new System.Windows.Forms.Label();
            primechanieLabel = new System.Windows.Forms.Label();
            nomIzvLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dBDocDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dopolnenieBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // nomerPPLabel
            // 
            nomerPPLabel.AutoSize = true;
            nomerPPLabel.Location = new System.Drawing.Point(10, 24);
            nomerPPLabel.Name = "nomerPPLabel";
            nomerPPLabel.Size = new System.Drawing.Size(34, 13);
            nomerPPLabel.TabIndex = 3;
            nomerPPLabel.Text = "П/П :";
            // 
            // dateLabel
            // 
            dateLabel.AutoSize = true;
            dateLabel.Location = new System.Drawing.Point(10, 80);
            dateLabel.Name = "dateLabel";
            dateLabel.Size = new System.Drawing.Size(39, 13);
            dateLabel.TabIndex = 5;
            dateLabel.Text = "Дата :";
            // 
            // sodIzmLabel
            // 
            sodIzmLabel.AutoSize = true;
            sodIzmLabel.Location = new System.Drawing.Point(10, 105);
            sodIzmLabel.Name = "sodIzmLabel";
            sodIzmLabel.Size = new System.Drawing.Size(137, 13);
            sodIzmLabel.TabIndex = 7;
            sodIzmLabel.Text = "Содержание Изменения :";
            // 
            // fIOLabel
            // 
            fIOLabel.AutoSize = true;
            fIOLabel.Location = new System.Drawing.Point(10, 131);
            fIOLabel.Name = "fIOLabel";
            fIOLabel.Size = new System.Drawing.Size(49, 13);
            fIOLabel.TabIndex = 9;
            fIOLabel.Text = "Ф.И.О. :";
            // 
            // primechanieLabel
            // 
            primechanieLabel.AutoSize = true;
            primechanieLabel.Location = new System.Drawing.Point(10, 157);
            primechanieLabel.Name = "primechanieLabel";
            primechanieLabel.Size = new System.Drawing.Size(76, 13);
            primechanieLabel.TabIndex = 11;
            primechanieLabel.Text = "Примечание :";
            // 
            // nomIzvLabel
            // 
            nomIzvLabel.AutoSize = true;
            nomIzvLabel.Location = new System.Drawing.Point(10, 51);
            nomIzvLabel.Name = "nomIzvLabel";
            nomIzvLabel.Size = new System.Drawing.Size(109, 13);
            nomIzvLabel.TabIndex = 15;
            nomIzvLabel.Text = "Номер Извещания :";
            // 
            // dBDocDataSet
            // 
            this.dBDocDataSet.DataSetName = "DBDocDataSet";
            this.dBDocDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dopolnenieBindingSource
            // 
            this.dopolnenieBindingSource.DataMember = "Dopolnenie";
            this.dopolnenieBindingSource.DataSource = this.dBDocDataSet;
            // 
            // dopolnenieTableAdapter
            // 
            this.dopolnenieTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.DocumTableAdapter = null;
            this.tableAdapterManager.DopolnenieTableAdapter = this.dopolnenieTableAdapter;
            this.tableAdapterManager.IzdelieTableAdapter = null;
            this.tableAdapterManager.TableDocumTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Documentaciya.DBDocDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // nomerPPTextBox
            // 
            this.nomerPPTextBox.Location = new System.Drawing.Point(150, 21);
            this.nomerPPTextBox.Name = "nomerPPTextBox";
            this.nomerPPTextBox.Size = new System.Drawing.Size(376, 20);
            this.nomerPPTextBox.TabIndex = 0;
            // 
            // dateDateTimePicker
            // 
            this.dateDateTimePicker.Location = new System.Drawing.Point(150, 76);
            this.dateDateTimePicker.Name = "dateDateTimePicker";
            this.dateDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.dateDateTimePicker.TabIndex = 2;
            // 
            // sodIzmTextBox
            // 
            this.sodIzmTextBox.Location = new System.Drawing.Point(150, 102);
            this.sodIzmTextBox.Name = "sodIzmTextBox";
            this.sodIzmTextBox.Size = new System.Drawing.Size(376, 20);
            this.sodIzmTextBox.TabIndex = 3;
            // 
            // fIOTextBox
            // 
            this.fIOTextBox.Location = new System.Drawing.Point(150, 128);
            this.fIOTextBox.Name = "fIOTextBox";
            this.fIOTextBox.Size = new System.Drawing.Size(376, 20);
            this.fIOTextBox.TabIndex = 4;
            // 
            // primechanieTextBox
            // 
            this.primechanieTextBox.Location = new System.Drawing.Point(150, 154);
            this.primechanieTextBox.Multiline = true;
            this.primechanieTextBox.Name = "primechanieTextBox";
            this.primechanieTextBox.Size = new System.Drawing.Size(376, 275);
            this.primechanieTextBox.TabIndex = 5;
            // 
            // nomIzvTextBox
            // 
            this.nomIzvTextBox.Location = new System.Drawing.Point(150, 48);
            this.nomIzvTextBox.Name = "nomIzvTextBox";
            this.nomIzvTextBox.Size = new System.Drawing.Size(376, 20);
            this.nomIzvTextBox.TabIndex = 1;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(451, 440);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSave.Location = new System.Drawing.Point(360, 440);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "ОК";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // frmDop
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(538, 477);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(nomerPPLabel);
            this.Controls.Add(this.nomerPPTextBox);
            this.Controls.Add(dateLabel);
            this.Controls.Add(this.dateDateTimePicker);
            this.Controls.Add(sodIzmLabel);
            this.Controls.Add(this.sodIzmTextBox);
            this.Controls.Add(fIOLabel);
            this.Controls.Add(this.fIOTextBox);
            this.Controls.Add(primechanieLabel);
            this.Controls.Add(this.primechanieTextBox);
            this.Controls.Add(nomIzvLabel);
            this.Controls.Add(this.nomIzvTextBox);
            this.Name = "frmDop";
            this.Text = "Edit";
            this.Load += new System.EventHandler(this.frmDop_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dBDocDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dopolnenieBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DBDocDataSet dBDocDataSet;
        private System.Windows.Forms.BindingSource dopolnenieBindingSource;
        private DBDocDataSetTableAdapters.DopolnenieTableAdapter dopolnenieTableAdapter;
        private DBDocDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox nomerPPTextBox;
        private System.Windows.Forms.DateTimePicker dateDateTimePicker;
        private System.Windows.Forms.TextBox sodIzmTextBox;
        private System.Windows.Forms.TextBox fIOTextBox;
        private System.Windows.Forms.TextBox primechanieTextBox;
        private System.Windows.Forms.TextBox nomIzvTextBox;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
    }
}