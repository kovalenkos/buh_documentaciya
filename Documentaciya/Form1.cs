﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Documentaciya
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            
        }

        public void SaveAll()
        {
            this.Validate();
            this.documBindingSource.EndEdit();
            this.dopolnenieBindingSource.EndEdit();
            this.izdelieBindingSource.EndEdit();
            this.tableDocumBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dBDocDataSet);
            return;
        }

        
        public void filtr()
        {
            if (comboBox1.SelectedValue != null)
            {
                string text = comboBox1.SelectedValue.ToString();
                documBindingSource.Filter = "idIzd = " + text;
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {
           
            // TODO: данная строка кода позволяет загрузить данные в таблицу "dBDocDataSet.Dopolnenie". При необходимости она может быть перемещена или удалена.
            this.dopolnenieTableAdapter.Fill(this.dBDocDataSet.Dopolnenie);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "dBDocDataSet.TableDocum". При необходимости она может быть перемещена или удалена.
            this.tableDocumTableAdapter.Fill(this.dBDocDataSet.TableDocum);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "dBDocDataSet.Docum". При необходимости она может быть перемещена или удалена.
            this.documTableAdapter.Fill(this.dBDocDataSet.Docum);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "dBDocDataSet.Izdelie". При необходимости она может быть перемещена или удалена.
            this.izdelieTableAdapter.Fill(this.dBDocDataSet.Izdelie);

            filtr();

            dopolnenieDataGridView.Columns[0].Visible = false;

        }

       
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            filtr();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveAll();
        }

        private void tableDocumDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if(tableDocumDataGridView.Columns[e.ColumnIndex].Name=="Delete")
            {
                if (MessageBox.Show("Вы уверены что хотите удалить?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                tableDocumBindingSource.RemoveCurrent();
            }
        }

        private void dopolnenieDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dopolnenieDataGridView.Columns[e.ColumnIndex].Name == "DeleteDop")
            {
                if (MessageBox.Show("Вы уверены что хотите удалить?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    dopolnenieBindingSource.RemoveCurrent();
            }
        }

        private void EditTab()
        {
            
                //выводит инфу с таблицы в новое окно
                var row = tableDocumDataGridView.CurrentRow;
                frmDocEdit frm = new frmDocEdit();
                var txt1 = frm.Controls["iNomTextBox"];
                var txt2 = frm.Controls["dateDateTimePicker"];
                var txt3 = frm.Controls["obozTextBox"];
                var txt4 = frm.Controls["kolLisTextBox"];
                var txt5 = frm.Controls["formatTextBox"];
                var txt6 = frm.Controls["naimenovanieTextBox"];
                var txt7 = frm.Controls["kemVipTextBox"];
                var txt8 = frm.Controls["kemPrinTextBox"];
                var txt9 = frm.Controls["primechanieTextBox"];

                txt1.Text = row.Cells[1].Value.ToString();
                txt2.Text = row.Cells[2].Value.ToString();
                txt3.Text = row.Cells[3].Value.ToString();
                txt4.Text = row.Cells[4].Value.ToString();
                txt5.Text = row.Cells[5].Value.ToString();
                txt6.Text = row.Cells[6].Value.ToString();
                txt7.Text = row.Cells[7].Value.ToString();
                txt8.Text = row.Cells[8].Value.ToString();
                txt9.Text = row.Cells[9].Value.ToString();

                //перемещает инфу с окна в таблицу
                if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    row.Cells[1].Value = txt1.Text;
                    row.Cells[2].Value = txt2.Text;
                    row.Cells[3].Value = txt3.Text;
                    row.Cells[4].Value = txt4.Text;
                    row.Cells[5].Value = txt5.Text;
                    row.Cells[6].Value = txt6.Text;
                    row.Cells[7].Value = txt7.Text;
                    row.Cells[8].Value = txt8.Text;
                    row.Cells[9].Value = txt9.Text;

                }
            
        }
        private void tableDocumDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (tableDocumDataGridView.CurrentCell.ColumnIndex == 10)
            {
                EditTab();
            }
          /*  if(tableDocumDataGridView.CurrentCell.ColumnIndex==10)
            {
                //выводит инфу с таблицы в новое окно
                var row = tableDocumDataGridView.CurrentRow;
                frmDocEdit frm = new frmDocEdit();
                var txt1 = frm.Controls["iNomTextBox"];
                var txt2 = frm.Controls["dateDateTimePicker"];
                var txt3 = frm.Controls["obozTextBox"];
                var txt4 = frm.Controls["kolLisTextBox"];
                var txt5 = frm.Controls["formatTextBox"];
                var txt6 = frm.Controls["naimenovanieTextBox"];
                var txt7 = frm.Controls["kemVipTextBox"];
                var txt8 = frm.Controls["kemPrinTextBox"];
                var txt9 = frm.Controls["primechanieTextBox"];

                txt1.Text = row.Cells[1].Value.ToString();
                txt2.Text = row.Cells[2].Value.ToString();
                txt3.Text = row.Cells[3].Value.ToString();
                txt4.Text = row.Cells[4].Value.ToString();
                txt5.Text = row.Cells[5].Value.ToString();
                txt6.Text = row.Cells[6].Value.ToString();
                txt7.Text = row.Cells[7].Value.ToString();
                txt8.Text = row.Cells[8].Value.ToString();
                txt9.Text = row.Cells[9].Value.ToString();

                //перемещает инфу с окна в таблицу
                if (frm.ShowDialog()==System.Windows.Forms.DialogResult.OK)
                {
                    row.Cells[1].Value = txt1.Text;
                    row.Cells[2].Value = txt2.Text;
                    row.Cells[3].Value = txt3.Text;
                    row.Cells[4].Value = txt4.Text;
                    row.Cells[5].Value = txt5.Text;
                    row.Cells[6].Value = txt6.Text;
                    row.Cells[7].Value = txt7.Text;
                    row.Cells[8].Value = txt8.Text;
                    row.Cells[9].Value = txt9.Text;

                }
            }*/
        }

      
        private void dopolnenieDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dopolnenieDataGridView.CurrentCell.ColumnIndex == 7)
                
            {
                //выводит инфу с таблицы в новое окно
                
                var row = dopolnenieDataGridView.CurrentRow;
                frmDop frm = new frmDop();
                var txt1 = frm.Controls["nomerPPTextBox"];
                var txt2 = frm.Controls["nomIzvTextBox"];
                var txt3 = frm.Controls["dateDateTimePicker"];
                var txt4 = frm.Controls["sodIzmTextBox"];
                var txt5 = frm.Controls["fIOTextBox"];
                var txt6 = frm.Controls["primechanieTextBox"];
               

                txt1.Text = row.Cells[1].Value.ToString();
                txt2.Text = row.Cells[2].Value.ToString();
                txt3.Text = row.Cells[3].Value.ToString();
                txt4.Text = row.Cells[4].Value.ToString();
                txt5.Text = row.Cells[5].Value.ToString();
                txt6.Text = row.Cells[6].Value.ToString();
               

                //перемещает инфу с окна в таблицу
                if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    row.Cells[1].Value = txt1.Text;
                    row.Cells[2].Value = txt2.Text;
                    row.Cells[3].Value = txt3.Text;
                    row.Cells[4].Value = txt4.Text;
                    row.Cells[5].Value = txt5.Text;
                    row.Cells[6].Value = txt6.Text;
                   

                }
            }
        }

        private void измененияИзделийToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmIzdeliy izd = new frmIzdeliy();
            
            izd.Show();
          
           
        }

      

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void сохранитьВсеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveAll();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            //tableDocumBindingSource.Filter = "INom = \'" + txtFind.Text + "\'";

            if (comboxSetting.Text == "")
            {
                MessageBox.Show("Вы не ввели значение для поиска!!!");
            }

            else if(comboxSetting.Text != null)
            {
                switch (comboxSetting.Text)
                {
                    case "Инвертарный номер":
                        tableDocumBindingSource.Filter = "INom = \'" + txtFind.Text + "\'";
                     
                        break;
                    case "Дата":
                        tableDocumBindingSource.Filter = "Date = \'" + txtFind.Text + "\'";
                        break;
                    case "Обозначение":
                        tableDocumBindingSource.Filter = "Oboz = \'" + txtFind.Text + "\'";
                        break;
                    case "Наименование":
                        tableDocumBindingSource.Filter = "Naimenovanie = \'" + txtFind.Text + "\'";
                        break;
                    case "Кем выпущено":
                        tableDocumBindingSource.Filter = "KemVip = \'" + txtFind.Text + "\'";
                        break;
                    case "Кем принят":
                        tableDocumBindingSource.Filter = "KemPrin = \'" + txtFind.Text + "\'";
                        break;
                }
                
            }
           
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            tableDocumBindingSource.Filter = null;
        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 about = new AboutBox1();
            about.ShowDialog();
        }

        private void tableDocumDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var row = tableDocumDataGridView.CurrentRow;
            frmDocEdit frm = new frmDocEdit();
            frm.Text = "Просмотр";
            var txt1 = frm.Controls["iNomTextBox"];
            var txt2 = frm.Controls["dateDateTimePicker"];
            var txt3 = frm.Controls["obozTextBox"];
            var txt4 = frm.Controls["kolLisTextBox"];
            var txt5 = frm.Controls["formatTextBox"];
            var txt6 = frm.Controls["naimenovanieTextBox"];
            var txt7 = frm.Controls["kemVipTextBox"];
            var txt8 = frm.Controls["kemPrinTextBox"];
            var txt9 = frm.Controls["primechanieTextBox"];
            var but1 = frm.Controls["btnSave"];
            var btn2 = frm.Controls["btnCancen"];
           
            but1.Visible = false;
            btn2.Visible = false;
            

            txt1.Text = row.Cells[1].Value.ToString();
            txt2.Text = row.Cells[2].Value.ToString();
            txt3.Text = row.Cells[3].Value.ToString();
            txt4.Text = row.Cells[4].Value.ToString();
            txt5.Text = row.Cells[5].Value.ToString();
            txt6.Text = row.Cells[6].Value.ToString();
            txt7.Text = row.Cells[7].Value.ToString();
            txt8.Text = row.Cells[8].Value.ToString();
            txt9.Text = row.Cells[9].Value.ToString();

            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                row.Cells[1].Value = txt1.Text;
                row.Cells[2].Value = txt2.Text;
                row.Cells[3].Value = txt3.Text;
                row.Cells[4].Value = txt4.Text;
                row.Cells[5].Value = txt5.Text;
                row.Cells[6].Value = txt6.Text;
                row.Cells[7].Value = txt7.Text;
                row.Cells[8].Value = txt8.Text;
                row.Cells[9].Value = txt9.Text;

            }
        }

        private void dopolnenieDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var row = dopolnenieDataGridView.CurrentRow;
            frmDop frm = new frmDop();
            frm.Text = "Просмотр";
            var txt1 = frm.Controls["nomerPPTextBox"];
            var txt2 = frm.Controls["nomIzvTextBox"];
            var txt3 = frm.Controls["dateDateTimePicker"];
            var txt4 = frm.Controls["sodIzmTextBox"];
            var txt5 = frm.Controls["fIOTextBox"];
            var txt6 = frm.Controls["primechanieTextBox"];
            var but1 = frm.Controls["btnSave"];
            var btn2 = frm.Controls["btnCancel"];

            but1.Visible = false;
            btn2.Visible = false;


            txt1.Text = row.Cells[1].Value.ToString();
            txt2.Text = row.Cells[2].Value.ToString();
            txt3.Text = row.Cells[3].Value.ToString();
            txt4.Text = row.Cells[4].Value.ToString();
            txt5.Text = row.Cells[5].Value.ToString();
            txt6.Text = row.Cells[6].Value.ToString();

            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                row.Cells[1].Value = txt1.Text;
                row.Cells[2].Value = txt2.Text;
                row.Cells[3].Value = txt3.Text;
                row.Cells[4].Value = txt4.Text;
                row.Cells[5].Value = txt5.Text;
                row.Cells[6].Value = txt6.Text;


            }

        }

        private void настройкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Setting setconf = new Setting();
            setconf.ShowDialog();
        }
    }
}
