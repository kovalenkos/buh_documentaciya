﻿namespace Documentaciya
{
    partial class frmDocEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label iNomLabel;
            System.Windows.Forms.Label dateLabel;
            System.Windows.Forms.Label obozLabel;
            System.Windows.Forms.Label kolLisLabel;
            System.Windows.Forms.Label formatLabel;
            System.Windows.Forms.Label naimenovanieLabel;
            System.Windows.Forms.Label kemVipLabel;
            System.Windows.Forms.Label kemPrinLabel;
            System.Windows.Forms.Label primechanieLabel;
            this.iNomTextBox = new System.Windows.Forms.TextBox();
            this.dateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.obozTextBox = new System.Windows.Forms.TextBox();
            this.kolLisTextBox = new System.Windows.Forms.TextBox();
            this.formatTextBox = new System.Windows.Forms.TextBox();
            this.naimenovanieTextBox = new System.Windows.Forms.TextBox();
            this.kemVipTextBox = new System.Windows.Forms.TextBox();
            this.kemPrinTextBox = new System.Windows.Forms.TextBox();
            this.primechanieTextBox = new System.Windows.Forms.TextBox();
            this.btnCancen = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.dBDocDataSet = new Documentaciya.DBDocDataSet();
            this.tableDocumBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tableDocumTableAdapter = new Documentaciya.DBDocDataSetTableAdapters.TableDocumTableAdapter();
            this.tableAdapterManager = new Documentaciya.DBDocDataSetTableAdapters.TableAdapterManager();
            iNomLabel = new System.Windows.Forms.Label();
            dateLabel = new System.Windows.Forms.Label();
            obozLabel = new System.Windows.Forms.Label();
            kolLisLabel = new System.Windows.Forms.Label();
            formatLabel = new System.Windows.Forms.Label();
            naimenovanieLabel = new System.Windows.Forms.Label();
            kemVipLabel = new System.Windows.Forms.Label();
            kemPrinLabel = new System.Windows.Forms.Label();
            primechanieLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dBDocDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableDocumBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // iNomLabel
            // 
            iNomLabel.AutoSize = true;
            iNomLabel.Location = new System.Drawing.Point(20, 25);
            iNomLabel.Name = "iNomLabel";
            iNomLabel.Size = new System.Drawing.Size(114, 13);
            iNomLabel.TabIndex = 3;
            iNomLabel.Text = "Инвертарный номер:";
            // 
            // dateLabel
            // 
            dateLabel.AutoSize = true;
            dateLabel.Location = new System.Drawing.Point(20, 52);
            dateLabel.Name = "dateLabel";
            dateLabel.Size = new System.Drawing.Size(36, 13);
            dateLabel.TabIndex = 5;
            dateLabel.Text = "Дата:";
            // 
            // obozLabel
            // 
            obozLabel.AutoSize = true;
            obozLabel.Location = new System.Drawing.Point(20, 77);
            obozLabel.Name = "obozLabel";
            obozLabel.Size = new System.Drawing.Size(77, 13);
            obozLabel.TabIndex = 7;
            obozLabel.Text = "Обозначение:";
            // 
            // kolLisLabel
            // 
            kolLisLabel.AutoSize = true;
            kolLisLabel.Location = new System.Drawing.Point(20, 103);
            kolLisLabel.Name = "kolLisLabel";
            kolLisLabel.Size = new System.Drawing.Size(107, 13);
            kolLisLabel.TabIndex = 9;
            kolLisLabel.Text = "Количество листов:";
            // 
            // formatLabel
            // 
            formatLabel.AutoSize = true;
            formatLabel.Location = new System.Drawing.Point(20, 129);
            formatLabel.Name = "formatLabel";
            formatLabel.Size = new System.Drawing.Size(52, 13);
            formatLabel.TabIndex = 11;
            formatLabel.Text = "Формат:";
            // 
            // naimenovanieLabel
            // 
            naimenovanieLabel.AutoSize = true;
            naimenovanieLabel.Location = new System.Drawing.Point(20, 155);
            naimenovanieLabel.Name = "naimenovanieLabel";
            naimenovanieLabel.Size = new System.Drawing.Size(86, 13);
            naimenovanieLabel.TabIndex = 13;
            naimenovanieLabel.Text = "Наименование:";
            // 
            // kemVipLabel
            // 
            kemVipLabel.AutoSize = true;
            kemVipLabel.Location = new System.Drawing.Point(20, 181);
            kemVipLabel.Name = "kemVipLabel";
            kemVipLabel.Size = new System.Drawing.Size(80, 13);
            kemVipLabel.TabIndex = 15;
            kemVipLabel.Text = "Кем выпущен:";
            // 
            // kemPrinLabel
            // 
            kemPrinLabel.AutoSize = true;
            kemPrinLabel.Location = new System.Drawing.Point(20, 207);
            kemPrinLabel.Name = "kemPrinLabel";
            kemPrinLabel.Size = new System.Drawing.Size(120, 13);
            kemPrinLabel.TabIndex = 17;
            kemPrinLabel.Text = "Кем принят документ:";
            // 
            // primechanieLabel
            // 
            primechanieLabel.AutoSize = true;
            primechanieLabel.Location = new System.Drawing.Point(20, 233);
            primechanieLabel.Name = "primechanieLabel";
            primechanieLabel.Size = new System.Drawing.Size(73, 13);
            primechanieLabel.TabIndex = 19;
            primechanieLabel.Text = "Примечание:";
            // 
            // iNomTextBox
            // 
            this.iNomTextBox.Location = new System.Drawing.Point(140, 22);
            this.iNomTextBox.Name = "iNomTextBox";
            this.iNomTextBox.Size = new System.Drawing.Size(362, 20);
            this.iNomTextBox.TabIndex = 0;
            // 
            // dateDateTimePicker
            // 
            this.dateDateTimePicker.Location = new System.Drawing.Point(140, 48);
            this.dateDateTimePicker.Name = "dateDateTimePicker";
            this.dateDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.dateDateTimePicker.TabIndex = 1;
            // 
            // obozTextBox
            // 
            this.obozTextBox.Location = new System.Drawing.Point(140, 74);
            this.obozTextBox.Name = "obozTextBox";
            this.obozTextBox.Size = new System.Drawing.Size(362, 20);
            this.obozTextBox.TabIndex = 2;
            // 
            // kolLisTextBox
            // 
            this.kolLisTextBox.Location = new System.Drawing.Point(140, 100);
            this.kolLisTextBox.Name = "kolLisTextBox";
            this.kolLisTextBox.Size = new System.Drawing.Size(362, 20);
            this.kolLisTextBox.TabIndex = 3;
            // 
            // formatTextBox
            // 
            this.formatTextBox.Location = new System.Drawing.Point(140, 126);
            this.formatTextBox.Name = "formatTextBox";
            this.formatTextBox.Size = new System.Drawing.Size(362, 20);
            this.formatTextBox.TabIndex = 4;
            // 
            // naimenovanieTextBox
            // 
            this.naimenovanieTextBox.Location = new System.Drawing.Point(140, 152);
            this.naimenovanieTextBox.Name = "naimenovanieTextBox";
            this.naimenovanieTextBox.Size = new System.Drawing.Size(362, 20);
            this.naimenovanieTextBox.TabIndex = 5;
            // 
            // kemVipTextBox
            // 
            this.kemVipTextBox.Location = new System.Drawing.Point(140, 178);
            this.kemVipTextBox.Name = "kemVipTextBox";
            this.kemVipTextBox.Size = new System.Drawing.Size(362, 20);
            this.kemVipTextBox.TabIndex = 6;
            // 
            // kemPrinTextBox
            // 
            this.kemPrinTextBox.Location = new System.Drawing.Point(140, 204);
            this.kemPrinTextBox.Name = "kemPrinTextBox";
            this.kemPrinTextBox.Size = new System.Drawing.Size(362, 20);
            this.kemPrinTextBox.TabIndex = 7;
            // 
            // primechanieTextBox
            // 
            this.primechanieTextBox.Location = new System.Drawing.Point(140, 233);
            this.primechanieTextBox.Multiline = true;
            this.primechanieTextBox.Name = "primechanieTextBox";
            this.primechanieTextBox.Size = new System.Drawing.Size(362, 242);
            this.primechanieTextBox.TabIndex = 8;
            // 
            // btnCancen
            // 
            this.btnCancen.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancen.Location = new System.Drawing.Point(427, 494);
            this.btnCancen.Name = "btnCancen";
            this.btnCancen.Size = new System.Drawing.Size(75, 23);
            this.btnCancen.TabIndex = 10;
            this.btnCancen.Text = "Отмена";
            this.btnCancen.UseVisualStyleBackColor = true;
            this.btnCancen.Click += new System.EventHandler(this.btnCancen_Click);
            // 
            // btnSave
            // 
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSave.Location = new System.Drawing.Point(331, 494);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "OK";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // dBDocDataSet
            // 
            this.dBDocDataSet.DataSetName = "DBDocDataSet";
            this.dBDocDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableDocumBindingSource
            // 
            this.tableDocumBindingSource.DataMember = "TableDocum";
            this.tableDocumBindingSource.DataSource = this.dBDocDataSet;
            // 
            // tableDocumTableAdapter
            // 
            this.tableDocumTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.DocumTableAdapter = null;
            this.tableAdapterManager.DopolnenieTableAdapter = null;
            this.tableAdapterManager.IzdelieTableAdapter = null;
            this.tableAdapterManager.TableDocumTableAdapter = this.tableDocumTableAdapter;
            this.tableAdapterManager.UpdateOrder = Documentaciya.DBDocDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // frmDocEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(529, 537);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancen);
            this.Controls.Add(iNomLabel);
            this.Controls.Add(this.iNomTextBox);
            this.Controls.Add(dateLabel);
            this.Controls.Add(this.dateDateTimePicker);
            this.Controls.Add(obozLabel);
            this.Controls.Add(this.obozTextBox);
            this.Controls.Add(kolLisLabel);
            this.Controls.Add(this.kolLisTextBox);
            this.Controls.Add(formatLabel);
            this.Controls.Add(this.formatTextBox);
            this.Controls.Add(naimenovanieLabel);
            this.Controls.Add(this.naimenovanieTextBox);
            this.Controls.Add(kemVipLabel);
            this.Controls.Add(this.kemVipTextBox);
            this.Controls.Add(kemPrinLabel);
            this.Controls.Add(this.kemPrinTextBox);
            this.Controls.Add(primechanieLabel);
            this.Controls.Add(this.primechanieTextBox);
            this.Name = "frmDocEdit";
            this.Text = "Add/Edit";
            this.Load += new System.EventHandler(this.frmDocEdit_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dBDocDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableDocumBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DateTimePicker dateDateTimePicker;
        private System.Windows.Forms.TextBox obozTextBox;
        private System.Windows.Forms.TextBox kolLisTextBox;
        private System.Windows.Forms.TextBox formatTextBox;
        private System.Windows.Forms.TextBox naimenovanieTextBox;
        private System.Windows.Forms.TextBox kemVipTextBox;
        private System.Windows.Forms.TextBox kemPrinTextBox;
        private System.Windows.Forms.TextBox primechanieTextBox;
        private System.Windows.Forms.Button btnCancen;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox iNomTextBox;
        private DBDocDataSet dBDocDataSet;
        private System.Windows.Forms.BindingSource tableDocumBindingSource;
        private DBDocDataSetTableAdapters.TableDocumTableAdapter tableDocumTableAdapter;
        private DBDocDataSetTableAdapters.TableAdapterManager tableAdapterManager;
    }
}