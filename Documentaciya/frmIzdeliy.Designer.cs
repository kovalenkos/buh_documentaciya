﻿namespace Documentaciya
{
    partial class frmIzdeliy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmIzdeliy));
            this.dBDocDataSet = new Documentaciya.DBDocDataSet();
            this.izdelieBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.izdelieTableAdapter = new Documentaciya.DBDocDataSetTableAdapters.IzdelieTableAdapter();
            this.tableAdapterManager = new Documentaciya.DBDocDataSetTableAdapters.TableAdapterManager();
            this.izdelieBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.izdelieBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.izdelieDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dBDocDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.izdelieBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.izdelieBindingNavigator)).BeginInit();
            this.izdelieBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.izdelieDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // dBDocDataSet
            // 
            this.dBDocDataSet.DataSetName = "DBDocDataSet";
            this.dBDocDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // izdelieBindingSource
            // 
            this.izdelieBindingSource.DataMember = "Izdelie";
            this.izdelieBindingSource.DataSource = this.dBDocDataSet;
            // 
            // izdelieTableAdapter
            // 
            this.izdelieTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.DocumTableAdapter = null;
            this.tableAdapterManager.DopolnenieTableAdapter = null;
            this.tableAdapterManager.IzdelieTableAdapter = this.izdelieTableAdapter;
            this.tableAdapterManager.TableDocumTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Documentaciya.DBDocDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // izdelieBindingNavigator
            // 
            this.izdelieBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.izdelieBindingNavigator.BindingSource = this.izdelieBindingSource;
            this.izdelieBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.izdelieBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.izdelieBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.izdelieBindingNavigatorSaveItem});
            this.izdelieBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.izdelieBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.izdelieBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.izdelieBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.izdelieBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.izdelieBindingNavigator.Name = "izdelieBindingNavigator";
            this.izdelieBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.izdelieBindingNavigator.Size = new System.Drawing.Size(264, 25);
            this.izdelieBindingNavigator.TabIndex = 0;
            this.izdelieBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Добавить";
            this.bindingNavigatorAddNewItem.Visible = false;
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(43, 22);
            this.bindingNavigatorCountItem.Text = "для {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Общее число элементов";
            this.bindingNavigatorCountItem.Visible = false;
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Удалить";
            this.bindingNavigatorDeleteItem.Visible = false;
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Переместить в начало";
            this.bindingNavigatorMoveFirstItem.Visible = false;
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Переместить назад";
            this.bindingNavigatorMovePreviousItem.Visible = false;
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            this.bindingNavigatorSeparator.Visible = false;
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Положение";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Текущее положение";
            this.bindingNavigatorPositionItem.Visible = false;
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            this.bindingNavigatorSeparator1.Visible = false;
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Переместить вперед";
            this.bindingNavigatorMoveNextItem.Visible = false;
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Переместить в конец";
            this.bindingNavigatorMoveLastItem.Visible = false;
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            this.bindingNavigatorSeparator2.Visible = false;
            // 
            // izdelieBindingNavigatorSaveItem
            // 
            this.izdelieBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.izdelieBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("izdelieBindingNavigatorSaveItem.Image")));
            this.izdelieBindingNavigatorSaveItem.Name = "izdelieBindingNavigatorSaveItem";
            this.izdelieBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.izdelieBindingNavigatorSaveItem.Text = "Сохранить данные";
            this.izdelieBindingNavigatorSaveItem.Click += new System.EventHandler(this.izdelieBindingNavigatorSaveItem_Click);
            // 
            // izdelieDataGridView
            // 
            this.izdelieDataGridView.AutoGenerateColumns = false;
            this.izdelieDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.izdelieDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.izdelieDataGridView.DataSource = this.izdelieBindingSource;
            this.izdelieDataGridView.Location = new System.Drawing.Point(12, 28);
            this.izdelieDataGridView.Name = "izdelieDataGridView";
            this.izdelieDataGridView.Size = new System.Drawing.Size(244, 257);
            this.izdelieDataGridView.TabIndex = 1;
            this.izdelieDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.izdelieDataGridView_CellContentClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.dataGridViewTextBoxColumn1.HeaderText = "Id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Name";
            this.dataGridViewTextBoxColumn2.HeaderText = "Изделия";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 200;
            // 
            // frmIzdeliy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(264, 292);
            this.Controls.Add(this.izdelieDataGridView);
            this.Controls.Add(this.izdelieBindingNavigator);
            this.MaximumSize = new System.Drawing.Size(280, 330);
            this.MinimumSize = new System.Drawing.Size(280, 330);
            this.Name = "frmIzdeliy";
            this.Text = "frmIzdeliy";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmIzdeliy_FormClosed);
            this.Load += new System.EventHandler(this.frmIzdeliy_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dBDocDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.izdelieBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.izdelieBindingNavigator)).EndInit();
            this.izdelieBindingNavigator.ResumeLayout(false);
            this.izdelieBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.izdelieDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DBDocDataSet dBDocDataSet;
        private System.Windows.Forms.BindingSource izdelieBindingSource;
        private DBDocDataSetTableAdapters.IzdelieTableAdapter izdelieTableAdapter;
        private DBDocDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator izdelieBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton izdelieBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView izdelieDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
    }
}