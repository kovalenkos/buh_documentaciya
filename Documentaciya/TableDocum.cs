//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Documentaciya
{
    using System;
    using System.Collections.Generic;
    
    public partial class TableDocum
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TableDocum()
        {
            this.Dopolnenie = new HashSet<Dopolnenie>();
        }
    
        public int Id { get; set; }
        public string INom { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string Oboz { get; set; }
        public string KolLis { get; set; }
        public string Format { get; set; }
        public string Naimenovanie { get; set; }
        public string KemVip { get; set; }
        public string KemPrin { get; set; }
        public string Primechanie { get; set; }
        public Nullable<int> idDocum { get; set; }
        public string Colours { get; set; }
    
        public virtual Docum Docum { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Dopolnenie> Dopolnenie { get; set; }
    }
}
